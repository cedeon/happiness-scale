import Vue from 'vue'
// import App from './App.vue'
import Scale from './Scale.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Scale),
}).$mount('#app')
